  
  
 #include "window.h"
  
 #include "mesh.h"
 #include "wavefront.h"  // pour charger un objet au format .obj
 #include "orbiter.h"
  
 #include "draw.h"        // pour dessiner du point de vue d'une camera

 #include <stdio.h>
 #include <vector>
#include <math.h>

using namespace std;
  
 Mesh objet;
 Orbiter camera;
 Mesh line(GL_LINES);
 Mesh line2(GL_LINES);

 vector<Point> subdivision(vector<Point> v) {
     
     vector<Point> sub;
     for (vector<Point>::iterator i = v.begin();
         i != v.end(); ++i) {
         if (i != v.begin()) {
             // 1/4
             Vector v1(*(i - 1), *i);
             Point p1 = *(i - 1) + 0.25 * v1;
             sub.push_back(p1);
             // 3/4
             Point p2 = *(i - 1) + 0.75 * v1;
             sub.push_back(p2);
         }
         if (i == v.begin() && i == v.end()) {
             sub.push_back(*i);
         }
     }

     return sub;
 }

 //Transform rotation_mat(const Vector&) {}

 int init( )
 {
     /************************ TP1 ******************************/
     // Points creation
     Point one(1., 0., 0.);
     Point two(0., 1., 0.);
     Point three(0., 0., 1.);
     Point four(1., 0., 1.);
     Point five(0.5, 1.5, 1.);

     vector<Point> points;
     points.push_back(one);
     points.push_back(two);
     points.push_back(three);
     points.push_back(four);
     points.push_back(five);
     points.push_back(one);
     
     //subdivision
     for (int i = 0; i < 5; i++) {
         points = subdivision(points);
     }


     // Orthogonal vectors
     /*Vector first(points[0], points[1]);
     Vector v_ortho = cross(first, Vector (1, 0, 0));
     if(length2(v_ortho) == 0){
         v_ortho = cross(first, Vector(0, 0, 1));
     }
     Vector norm = normalize(v_ortho);

     Vector second(points[1], points[2]);*/

     //float norm_first = length(first);
     //float norm_second = length(second);

     //float cos = dot(first,second) / (norm_first * norm_second);
     //float c = dot(first, second) * cos;
     /*
     float angle = acos(cos);
     float s = length(v_ortho) * sin(angle);
     */

     //float eq = 1 / (1 + c);

     // angle of the tow vectors     
     //Transform r; // Rotation Matrix

     //for (int i = 0; i < 4; ++i) {
     //    for (int j = 0; j < 4; ++j) {
     //        if(i == j)
     //           r.m[i][j] = 1;
     //        else
     //           r.m[i][j] = 0;
     //    }
     //}

     //r.m[0][1] = -v_ortho.z + pow(v_ortho.z, 2) * eq;
     //r.m[0][2] = v_ortho.y + pow(v_ortho.y, 2) * eq;
     //r.m[1][0] = v_ortho.z + pow(v_ortho.z, 2) * eq;
     //r.m[1][2] = -v_ortho.x + pow(v_ortho.x, 2) * eq;
     //r.m[2][0] = -v_ortho.y + pow(v_ortho.y, 2) * eq;
     //r.m[2][1] = v_ortho.x + pow(v_ortho.x, 2) * eq;
          
     
     //Vector norm = normalize(v_ortho);

     Transform rotation;

     for (vector<Point>::iterator i = points.begin();
         i != points.end(); ++i) {
         if (i != points.begin() && i != points.end()) {
             line.vertex(*i);
             line.vertex(*i);
         }
         else {
             line.vertex(*i);
         }
     }

     for (vector<Point>::iterator i = points.begin(); i != points.end() - 2; ++i) {
         Vector ve_1(*i, *(i+1));
         Vector ve_2(*(i + 1), *(i + 2));

         Vector v_ortho = cross(ve_1, Vector(1, 0, 0));
         if (length2(v_ortho) == 0) {
             v_ortho = cross(ve_1, Vector(0, 0, 1));
         }

         v_ortho = normalize(v_ortho);

         rotation = Rotation(ve_1, ve_2);
         line2.vertex(*i);
         v_ortho = rotation(v_ortho);
         line2.vertex(0.1 * v_ortho + (*i));

     }
     

     // etape 1 : charger un objet
     objet= read_mesh("data/flat_bbox.obj");
  
     // etape 2 : creer une camera pour observer l'objet
     // construit l'englobant de l'objet, les extremites de sa boite englobante
     Point pmin, pmax;
     objet.bounds(pmin, pmax);
  
     // regle le point de vue de la camera pour observer l'objet
     camera.lookat(pmin, pmax);
  
     // etat openGL par defaut
     glClearColor(0.2f, 0.2f, 0.2f, 1.f);        // couleur par defaut de la fenetre
  
     // etape 3 : configuration du pipeline.
 /* pour obtenir une image correcte lorsque l'on dessine plusieurs triangles, il faut choisir lequel conserver pour chaque pixel...
     on conserve le plus proche de la camera, celui que l'on voit normalement. ce test revient a considerer que les objets sont opaques.
  */
     glClearDepth(1.f);                          // profondeur par defaut
     glDepthFunc(GL_LESS);                       // ztest, conserver l'intersection la plus proche de la camera
     glEnable(GL_DEPTH_TEST);                    // activer le ztest
  
     return 0;   // ras, pas d'erreur
 }

 int draw( )
 {
     // etape 2 : dessiner l'objet avec opengl
  
     // on commence par effacer la fenetre avant de dessiner quelquechose
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     // on efface aussi le zbuffer
  
     // on dessine le triangle du point de vue de la camera
     //draw(objet, camera);
     draw(line, camera);
     draw(line2, camera);
  
     //return 1;   // on continue, renvoyer 0 pour sortir de l'application
  
 // on peut aussi controler la camera avec les mouvements de la souris
 
     // recupere les mouvements de la souris, utilise directement SDL2
     int mx, my;
     unsigned int mb= SDL_GetRelativeMouseState(&mx, &my);
  
     // deplace la camera
     if(mb & SDL_BUTTON(1))              // le bouton gauche est enfonce
         // tourne autour de l'objet
         camera.rotation(mx, my);
  
     else if(mb & SDL_BUTTON(3))         // le bouton droit est enfonce
         // approche / eloigne l'objet
         camera.move(mx);
  
     else if(mb & SDL_BUTTON(2))         // le bouton du milieu est enfonce
         // deplace le point de rotation
         camera.translation((float) mx / (float) window_width(), (float) my / (float) window_height());
  

     draw(line, camera);
     draw(line2, camera);

     //draw(objet, camera);
  
     return 1;
  
 }
  
 int quit( )
 {
     // etape 3 : detruire la description du triangle
     objet.release();
     return 0;   // ras, pas d'erreur
 }
  
  
 int main( int argc, char **argv )
 {
     // etape 1 : creer la fenetre
     Window window= create_window(1024, 640);
     if(window == NULL)
         return 1;
  
     // etape 2 : creer un contexte opengl pour pouvoir dessiner
     Context context= create_context(window);
     if(context == NULL)
         return 1;
  
     // etape 3 : creation des objets
     if(init() < 0)
     {
         printf("[error] init failed.\n");
         return 1;
     }
  
     // etape 4 : affichage de l'application, tant que la fenetre n'est pas fermee. ou que draw() ne renvoie pas 0
     run(window, draw);
  
     // etape 5 : nettoyage
     quit();
     release_context(context);
     release_window(window);
     return 0;
 }